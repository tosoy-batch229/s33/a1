// Create a fetch request using the GET method that will retrieve all the todo list items from JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => {
        console.log(json);

        // Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.
        const mapJSON = json.map((index) => {
            return index.title;
        });
        console.log(mapJSON);
});

// Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
// const choosen = 1;
fetch(`https://jsonplaceholder.typicode.com/todos/1`)
.then((response) => response.json())
.then((json) => {
    console.log(json)

    // Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.
    console.log(`The item "${json.title}" on the list has a status of ${json.completed}`);
});


// 7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos', {
    method : "POST",
    headers : {
        "Content-Type" : "application/json"
    },
    body : JSON.stringify({
        title : "Created To Do List Item",
        completed : false
    })
})
.then((response) => response.json())
.then((json) => {
    console.log(json);
});


// Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
fetch(`https://jsonplaceholder.typicode.com/todos/1`, {
    method: 'PUT',
    headers: {
        'Content-type': 'application/json; charset=UTF-8'
    },
    body: JSON.stringify({
        dateCompleted : "Pending",
        description: "To update the my to do list with a different data structure.",
        status : "Pending",
        title : "Updated To Do List Item",
        userId : 1
    })
})
    .then(response => response.json())
    .then(data => {
        console.log(data);
});


// Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
fetch(`https://jsonplaceholder.typicode.com/todos/1`, {
    method : 'DELETE',
})
.then(response => response.json())
.then(json => console.log(json));